#!/usr/bin/ruby

	require 'net/ftp'
	require 'ostruct'
	require 'json'

	class Mover

		attr_accessor :my
		@my = OpenStruct.new

		# @brief regexp'd stat
		# @return MatchData
		def self.stat entry

			return entry.match @my.expr

		end

		# @brief connect to origin and remote servers
		def self.setup origin, remote

			# regexp for stat
			@my.expr = /^(?<type>.{1})(?<mode>\S+)\s+(?<number>\d+)\s+(?<owner>\S+)\s+(?<group>\S+)\s+(?<size>\d+)\s+(?<mod_time>.{12})\s+(?<path>.+)$/

			# origin connection
			@my.origin = Net::FTP.new origin['host']
				@my.origin.login origin['user'], origin['pass']
				@my.origin.chdir origin['init']

			# remote connection
			@my.remote = Net::FTP.new remote['host']
				@my.remote.login remote['user'], remote['pass']

			# dirlist
			@my.dirs = ( Mover.dirs @my.origin.pwd, @my.origin.pwd )

			# make dirlist on remote
			#Mover.reconstruct @my.dirs

			# try to sync files
			Mover.sync @my.dirs

		end

		# @brief no need for uper dirs in list
		# @return array
		def self.listjunk

			return ['.', '..']

		end

		# @brief list dir entries, take subdirs
		# @return array
		def self.dirs dirname, prefix

			dirs = [ dirname ]
			
			@my.origin.list( dirname ).each do |entry|

				entry = stat entry

				if entry.class != MatchData
					break
				end

				if entry[:type] == 'd' &&
					!listjunk.include?( entry[:path] )

					now = "#{prefix}/#{entry[:path]}"
					
					dirs.push now
					dirs.push Mover.dirs now, now

				end

			end

			return dirs.flatten

		end

		# @brief clone on remote dir got on origin
		def self.reconstruct dirs

			dirs.each do |path|

				begin
				
					@my.remote.mkdir path

				rescue => e

					puts "Failed creating #{path}, #{e.inspect}"

				end

			end

		end

		# @brief determine whether entry really exists, based on FTP stat result
		# @return bool
		def self.exists stat

			return stat.split("\n").count > 2

		end

		# @brief synchronize files between origin and remote
		def self.sync dirs

			threads = []
		
			# look at dir
            dirs.each do |path|

            	sleep( 1 * rand( 0.50..1.50 ) )

				#threads << Thread.new do

					puts "\n\n #####"
					puts "Working #{path}"
					puts "#{dirs.index path} / #{dirs.count}"

					# find dir entries
					entries = @my.origin.list( path ).map { |e| stat( e ) }
						.reject { |e| e.class != MatchData } # remove non-entries
						.select { |e| e[:type] == '-' } # select file entries
						.map { |e| e[:path] }

					# sync each file
					entries.each do |filename|

					begin

						filepath = "#{path}/#{filename}"
						puts filepath

						begin

							timestamp = @my.origin.mdtm filepath

						rescue => e

							puts "Origin file can't be mdtm'd #{e.inspect}"
							
							timestamp = 0
							break

						end

						begin

							previous = @my.remote.mdtm filepath

						rescue => e

							puts "Remote file doesn't exist."
							previous = 0

						end

						if previous == 0 || (timestamp > previous)
						
							puts "Updating file"

							@my.origin.get filepath
							@my.remote.put filename, filepath

							File.delete filename

						end

					rescue => e

						next

					end

					end

				#end

			end

			# wait for each thread to finish
			#threads.each do |t|
#
#				t.join
#				puts "joined thread #{t}"
#
#				sleep 0.1
#
#			end

		end
	end

	conf = JSON.parse File.read 'servers.json'
	
	Mover.setup conf['origin'], conf['remote']